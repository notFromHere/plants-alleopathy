// Vuex plants module actions

import firebase from 'firebase';
import { firebaseConfig } from '../../../../firebase/firebaseConfig';
import { FILL_LIST } from './mutations-types';

firebase.initializeApp(firebaseConfig);

const fireStore = firebase.firestore();

export default {
  async getPlants({ commit }) {
    try {
      const records = (await fireStore.collection('plants').get());
      commit(FILL_LIST, records.docs.map((doc) => doc.data()));
    } catch (e) {
      console.log(e);
    }
  },
};
