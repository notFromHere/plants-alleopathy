export default [
  {
    path: '/',
    name: 'Platform',
    component: () => import(/* webpackChunkName: "Platform" */'@/views/Platform'),
    children: [
      {
        path: '/planting',
        name: 'PlatformPlanting',
        component: () => import(/* webpackChunkName: "PlatformPlanting" */'@/views/PlatformPlanting'),
      },
      {
        path: '/crop-rotation',
        name: 'PlatformCropRotation',
        component: () => import(/* webpackChunkName: "PlatformCropRotation" */'@/views/PlatformCropRotation'),
      },
    ],
  },
];
