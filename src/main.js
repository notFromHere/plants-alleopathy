import Vue from 'vue';
import ElementUI from 'element-ui';
import { dom, library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import axios from 'axios';
import 'firebase/auth';
import App from './App';
import './registerServiceWorker';
import router from './router';
import store from './store';
import 'element-ui/lib/theme-chalk/index.css';

import './assets/styles/style.sass';

Vue.prototype.$axios = axios;
Vue.prototype.$EventBus = new Vue();
Vue.config.productionTip = false;

Vue.use(ElementUI);
dom.watch();
library.add(fas);

Vue.component('icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
