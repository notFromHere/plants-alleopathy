export default {
  name: 'TheNavigation',

  props: [],

  data() {
    return {};
  },

  computed: {},

  methods: {
    handleRoute(path, params = {}) {
      this.$router.push({ path, params }).then();
    },
  },
};
