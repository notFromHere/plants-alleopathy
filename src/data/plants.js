export const plants = {
  plant: [
    {
      name: 'Brokuł',
      match: ['Brukselka', 'Jarmuż', 'Pasternak', 'Pietruszka', 'Por', 'Seler', 'Ziemniak'],
    },
    {
      name: 'Brukselka',
      match: ['Brokuł'],
    },
    {
      name: 'Burak ćwikłowy',
      match: ['Cebula', 'Cykoria', 'Groch', 'Kalarepa', 'Kapusta pekińska', 'Koper', 'Ogórek',
        'Pomidor', 'Por', 'Rzodkiewka', 'Sałata', 'Seler'],
    },
    {
      name: 'Cebula',
      match: ['Burak ćwikłowy', 'Cukinia', 'Cykoria', 'Endywia', 'Kalarepa', 'Koper', 'Marchew',
        'Ogórek', 'Pasternak', 'Pomidor', 'Por', 'Sałąta'],
    },
    {
      name: 'Cukinia',
      match: ['Cebula', 'Groch', 'Kukurydza', 'Pomidor', 'Szpinak'],
    },
    {
      name: 'Cykoria',
      match: ['Burak ćwikłowy', 'Cebula', 'Kalarepa', 'Koper', 'Marchew', 'Pomidor'],
    },
    {
      name: 'Czosnek',
      match: ['Marchew', 'Pomidor'],
    },
    {
      name: 'Dynia',
      match: ['Fasola', 'Kukurydza'],
    },
    {
      name: 'Endywia',
      match: ['Cebula', 'Fasola', 'Kalarepa', 'Kapusta pekińska', 'Koper', 'Marchew', 'Pomidor',
        'Por', 'Rzodkiewka'],
    },
    {
      name: 'Fasola',
      match: ['Dynia', 'Endywia', 'Kalarepa', 'Kukurydza', 'Ogórek', 'Rzepa', 'Seler', 'Szpinak',
        'Ziemniak'],
    },
    {
      name: 'Groch',
      match: ['Burak ćwikłowy', 'Cukinia', 'Kalarepa', 'Kapusta pekińska', 'Kukurydza', 'Marchew',
        'Ogórek', 'Rzepa', 'Rzodkiewka', 'Seler', 'Szpinak'],
    },
    {
      name: 'Jarmuż',
      match: ['Brokuł', 'Kapusta pekińska'],
    },
    {
      name: 'Kalarepa',
      match: ['Burak ćwikłowy', 'Cebula', 'Cykoria', 'Endywia', 'Fasola', 'Groch', 'Marchew',
        'Ogórek', 'Pomidor', 'Por', 'Rzodkiewka', 'Seler', 'Szparag', 'Szpinak', 'Ziemniak'],
    },
    {
      name: 'Kapusta pekińska',
      match: ['Burak ćwikłowy', 'Endywia', 'Groch', 'Jarmuż', 'Koper', 'Marchew', 'Papryka',
        'Pietruszka', 'Pomidor', 'Seler', 'Szpinak'],
    },
    {
      name: 'Koper',
      match: ['Burak ćwikłowy', 'Cebula', 'Cykoria', 'Endywia', 'Kapusta pekińska', 'Marchew',
        'Ogórek', 'Pietruszka', 'Seler', 'Szparag', 'Ziemniak'],
    },
    {
      name: 'Kukurydza',
      match: ['Cukinia', 'Dynia', 'Fasola', 'Groch', 'Ogórek', 'Pomidor', 'Ziemniak'],
    },
    {
      name: 'Marchew',
      match: ['Ceubla', 'Cykoria', 'Czosnek', 'Endywia', 'Groch', 'Kalarepa', 'Kapusta pekińska',
        'Koper', 'Marchew', 'Por', 'Rzodkiewka', 'Sałata', 'Seler', 'Szczypiorek', 'Szpinak'],
    },
    {
      name: 'Ogórek',
      match: ['Burak ćwikłowy', 'Cebula', 'Fasola', 'Groch', 'Kalarepa', 'Koper', 'Kukurydza',
        'Marchew', 'Papryka', 'Por', 'Rzodkiewka', 'Seler', 'Szpinak'],
    },
    {
      name: 'Papryka',
      match: ['Kapusta pekińska', 'Ogórek', 'Pomidor'],
    },
    {
      name: 'Psternak',
      match: ['Brokuł', 'Cebula', 'Rzepa', 'Rzodkiewka', 'Seler', 'Szpinak', 'Ziemniak'],
    },
    {
      name: 'Pietruszka',
      match: ['Brokuł', 'Kapusta pekińska', 'Koper', 'Pomidor', 'Por', 'Rzodkiewka'],
    },
    {
      name: 'Pomidor',
      match: ['Burak ćwikłowy', 'Cebula', 'Cukinia', 'Cykoria', 'Czosnek', 'Endywia', 'Kalarepa',
        'Kapusta pekińska', 'Kukurydza', 'Papryka', 'Pietruszka', 'Por', 'Rzepa', 'Rzodkiewka',
        'Seler', 'Szpinak'],
    },
    {
      name: 'Por',
      match: ['Brokuł', 'Burak ćwikłowy', 'Cebula', 'Endywia', 'Kalarepa', 'Marchew', 'Ogórek',
        'Pietruszka', 'Pomidor', 'Rzodkiewka', 'Sałata', 'Seler', 'Skorzonera', 'Szpinak'],
    },
    {
      name: 'Rzepa',
      match: ['Fasola', 'Groch', 'Pasternak', 'Pomidor', 'Seler', 'Szpinak'],
    },
    {
      name: 'Rzodkiewka',
      match: ['Burak ćwikłowy', 'Endywia', 'Groch', 'Kalarepa', 'Marchew', 'Ogórek', 'Pasternak',
        'Pietruszka', 'Pomidor', 'Por', 'Szpinak'],
    },
    {
      name: 'Sałata',
      match: ['Burak ćwikłowy', 'Cebula', 'Marchew', 'Por', 'Szparag'],
    },
    {
      name: 'Seler',
      match: ['Brokuł', 'Burak ćwikłowy', 'Fasola', 'Groch', 'Kalarepa', 'Kapusta pekińska',
        'Koper', 'Marchew', 'Ogórek', 'Pasternak', 'Pomidor', 'Por', 'Rzepa', 'Szpinak'],
    },
    {
      name: 'Skorzonera',
      match: ['Por'],
    },
    {
      name: 'Szpinak',
      match: ['Szczypiorek'],
    },
    {
      name: 'Szparag',
      match: ['Kalarepa', 'Koper', 'Sałata'],
    },
    {
      name: 'Szpinak',
      match: ['Cukinia', 'Fasola', 'Groch', 'Kalarepa', 'Kapusta pekińska', 'Marchew', 'Ogórek',
        'Pasternak', 'Pomidor', 'Popmidor', 'Por', 'Rzepa', 'Rzodkiewka', 'Seler', 'Ziemniak'],
    },
    {
      name: 'Ziemniak',
      match: ['Brokuł', 'Fasola', 'Kalarepa', 'Koper', 'Kukurydza', 'Pasternak', 'Szpinak'],
    },
  ],
};
