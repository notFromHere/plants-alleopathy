// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path');
const CompressionPlugin = require('compression-webpack-plugin');

const sassResources = [
  '@import "@/assets/styles/defs/__colors.sass"',
  '@import "@/assets/styles/defs/__global.sass"',
  '@import "@/assets/styles/mixins/__mixins.sass"',
];

const resolve = (dir) => path.join(__dirname, dir);

module.exports = {
  configureWebpack: {
    plugins: [new CompressionPlugin()],
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@$': resolve('src'),
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: sassResources.join(';'),
        sassOptions: {
          includePaths: [
            path.resolve(__dirname, 'src/core/'),
          ],
          indentedSyntax: true,
        },
      },
    },
  },
  assetsDir: '@/assets/',
};
