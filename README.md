# explorer

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Fontawesome (vue-fontawesome)
Normal icon
```
<icon icon="coffee" />
```
Spinners
```
<icon class="fa-spin"
      icon="spinner"
/>
```
